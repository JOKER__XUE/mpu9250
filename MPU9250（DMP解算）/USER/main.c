#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "led.h"
#include "Timer.h"
#include "mpu9250.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h"


char tmp_buf[33];			//字符串数组
float pitch,roll,yaw;                                    		//欧拉角:俯仰角，偏航角，滚转角
short gyrox,gyroy,gyroz;	//陀螺仪原始数据  gyroscope
short aacx,aacy,aacz;		//加速度传感器原始数据  angular acceleration
short temp;					//参量
u8 error;                   //错误值
//extern int i_num;
int main(void);				//主函数
void SYS_Init(void);		//系统初始化总函数	

int main(void)
{
	SYS_Init();                                      //系统初始化总函数
	while(1)                                         //主循环
	{
		//可在该位置填写需要做的任务
		delay_ms(100);                               //延时
	}
}

void SYS_Init(void)
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);  //中断优先级分组函数
	delay_init();								     //延时函数初始化
	uart_init(115200);	 	                         //串口初始化为115200 
	LED_Init();                                      //初始化与LED连接的硬件接口
	if(MPU_Init() == 0)			                     //初始化MPU9250
		printf("ID读取正常\r\n");                    //ID读取正常
	else
		printf("ID读取不正常\r\n");                  //ID读取不正常
	while(1)                                       
	{
		error = mpu_dmp_init();                      //初始化mpu_dmp库
		printf("ERROR:%d\r\n",error);                //串口显示初始化错误值
		switch (error)                               //对不同错误值类型判断
		{
			case 0:printf("DMP库初始化正常\r\n");break;
			case 1:printf("设置传感器失败\r\n");break;
			case 2:printf("设置FIFO失败\r\n");break;
			case 3:printf("设置采样率失败\r\n");break;
			case 4:printf("加载dmp固件失败\r\n");break;
			case 5:printf("设置陀螺仪方向失败\r\n");break;
			case 6:printf("设置dmp功能失败\r\n");break;
			case 7:printf("设置DMP输出速率失败\r\n");break;
			case 8:printf("自检失败\r\n");break;
			case 9:printf("使能DMP失败\r\n");break;
			case 10:printf("初始化MPU6050失败\r\n");break;
			default :printf("未知错误\r\n");break;
		}
		if(error == 0)break;                        //如果没有错误直接退出循环
		delay_ms(200);                              //延时
	} 
	
	TIM3_Int_Init(49,7199);					        //200hz的计数频率，计数到50为5ms 
	while(1){
	}
	
	//	while(1)
//	{
//		if(i_num==10000)
//	}	
}	

