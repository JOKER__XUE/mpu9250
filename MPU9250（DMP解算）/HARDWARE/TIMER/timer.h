#ifndef __TIMER_H
#define __TIMER_H
#include "sys.h"
#include "mpu9250.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h"

extern char tmp_buf[33];			//字符串数组
extern float pitch,roll,yaw; 		//欧拉角:俯仰角，偏航角，滚转角
extern short aacx,aacy,aacz;		//加速度传感器原始数据  angular acceleration
extern short gyrox,gyroy,gyroz;	//陀螺仪原始数据  gyroscope
extern short temp;					//温度


void TIM3_Int_Init(u16 arr,u16 psc);	
 
#endif
